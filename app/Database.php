<?php
namespace App;

class Database {
    private static $instance = null;
    private $consoles = [];
    private $shelfArea = 0;
    private $usedArea = 0; 
    
    public static function getInstance(): Database {
        if(Database::$instance == null) Database::$instance = new Database();
        return Database::$instance;
    }

    public function getConsoles(): Array
    {
        return $this->consoles;
    }

    public function setConsoles($consoles): Void
    {
        $this->consoles = $consoles;
    }

    public function getShelfArea(): int
    {
        return $this->shelfArea;
    }

    public function setShelfArea($shelfArea): Void
    {
        $this->shelfArea = $shelfArea;
    }

    public function getUsedfArea(): int
    {
        return $this->usedArea;
    }

    public function setUsedfArea($usedArea): Void
    {
        $this->usedArea = $usedArea;
    }
}