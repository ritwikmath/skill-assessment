<?php
namespace App;

trait Cube {
    public function calculateArea(): int
    {
        return (2*$this->height*$this->width)+(2*$this->depth*$this->height)+(2*$this->depth*$this->width);
    }
}