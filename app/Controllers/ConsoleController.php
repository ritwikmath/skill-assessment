<?php
namespace App\Controllers;

use App\Models\Console;
use App\Models\High;
use App\Models\Low;
use App\Models\Medium;
use Exception;

class ConsoleController {
    private $console;
    
    public function __construct($type = "") {
        if ($type == "low") {
            $this->console = new Low();
        } elseif ($type == "medium") {
            $this->console = new Medium();
        } elseif($type == "high") {
            $this->console = new High();
        } else {
            $this->console = new Console();
        }
    }

    public function storedConsoles(): Array
    {
       try {
            return $this->console->getAllConsoles();
       } catch (Exception $e) {
            echo("{$e->getMessage()}\n");
       }
    }

    public function storeConsoleOnShelf($numbers): void {
        try { 
            while($numbers > 0) {
                $this->console->store();
                $numbers--;
            }
        } catch (Exception $e) {
                echo("{$e->getMessage()}\n");
        }
    }

    public function removeConsoleFromShelf($position): void
    {
        try { 
            $this->console->remove($position); 
        } catch (Exception $e) {
            echo("{$e->getMessage()}\n");
       }
    }

    public function getConsoleCost(): int
    {
        try {
            $consoles = $this->console->getAllConsoles();
            $cost = 0;
            foreach($consoles as $console) {
                $cost += $console["price"];
            }
            return $cost;
        } catch (Exception $e) {
            echo("{$e->getMessage()}\n");
        }
    }

    public function getConsoleCostByCategory($type): int
    {
        try {
            $consoles = $this->console->getAllConsoles();
            $cost = 0;
            foreach($consoles as $console) {
                if ($console["type"] == $type) $cost += $console["price"];
            }
            return $cost;
        } catch (Exception $e) {
            echo("{$e->getMessage()}\n");
        }
    }
}