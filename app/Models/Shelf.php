<?php
namespace App\Models;

use App\Cube;
use App\Database;

class Shelf {
    use Cube;

    public $height = 2000, $width = 1000, $depth = 210;

    public function __construct() {
        $db = Database::getInstance();
        $area = $db->getShelfArea() + $this->calculateArea();
        $db->setShelfArea($area);
    }
}