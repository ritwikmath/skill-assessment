<?php
namespace App\Models;

use App\Models\Console;

class Medium extends Console {
    public $height = 120, $width = 354, $depth = 280, $price = 639, $type = "MegaSuperPlay I";
}