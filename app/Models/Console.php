<?php
namespace App\Models;

use App\Cube;
use App\Database;
use Exception;

class Console {
    use Cube;
    
    protected $db;

    public function __construct() {
        $this->db = Database::getInstance();
    }

    public function store(): Void {
        $area = $this->calculateArea();
        $usedArea = $this->db->getUsedfArea();
        $totalShelfArea = $this->db->getShelfArea();

        if ($totalShelfArea < ($usedArea+$area)) throw new Exception("Shelf area exceeded");

        $console  = [
            "type" => $this->type,
            "price" => $this->price,
            "area" => $area
        ];

        $consoles = $this->db->getConsoles(); 
        array_push($consoles, $console);

        $this->db->setConsoles($consoles);
        $this->db->setUsedfArea($area+$usedArea);
    }

    public function remove($position): Void {
        $consoles = $this->db->getConsoles();
        $area = $consoles[$position]["area"];
        $usedArea = $this->db->getUsedfArea();
        array_splice($consoles, $position, 1);
        $this->db->setConsoles($consoles);
        $this->db->setUsedfArea($usedArea-$area);
    }

    public function getAllConsoles(): Array {
        $consoles = $this->db->getConsoles();
        return $consoles;
    }
}