<?php

namespace App;

use App\Controllers\ConsoleController;
use App\Controllers\ShelfController;

require_once realpath("vendor/autoload.php");

$shelf = new ShelfController();
$shelf->add();

$console = new ConsoleController("low");
$console->storeConsoleOnShelf(10);

$console = new ConsoleController("medium");
$console->storeConsoleOnShelf(10);

$console = new ConsoleController("high");
$console->storeConsoleOnShelf(10);

$console = new ConsoleController();
$totalCost = $console->getConsoleCost();
echo "Total Cost: $"."{$totalCost}\n";
$categoryCost = $console->getConsoleCostByCategory("MegaSuperPlay I");
echo "Cost by Type: MegaSuperPlay I - $"."{$categoryCost}\n";
$console->removeConsoleFromShelf(1);